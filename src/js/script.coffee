class SearchPage

  constructor: ->

    # Initialize DOM reference
    @initDom()

    $ =>

      # Initialize a new Flickr Search instance with
      # a valid Flickr API key, AND a unique set of 
      # DOM wrapper elements for:
      # 1. Search input
      # 2. Carousel
      # 3. Results grid wrapper
      # 4. Pagination wrapper
      # --
      @FlickrSearch = new FlickrSearch {
        api_key:    'd2011d602fa5b6d6f61e09a72457fc12'
        search:     @search1
        carousel:   @carousel1
        list:       @list1
        pagination: @pagination1
      }

  initDom: =>
    @search1     = $('#search-1')
    @carousel1   = $('#carousel-1')
    @list1       = $('#results-grid-1')
    @pagination1 = $('#pagination-1')

@SearchPage = new SearchPage