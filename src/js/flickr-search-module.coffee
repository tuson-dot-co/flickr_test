class @FlickrSearch

  constructor: ({@api_key, @search, @carousel, @list, @pagination, @page_size}) ->

    # If not defined, throw errors or set to off:
    if @api_key.length isnt 32 or typeof @api_key isnt 'string'
      return throw "You need to provide a valid Flickr Api Key!"

    if !@search or @search.length is 0
      return throw "You need to provide a valid search input wrapper reference."      

    if !@carousel or @carousel.length is 0
      return throw "You need to provide a valid image carousel wrapper reference."
    
    if !@list or @list.length is 0
      return throw "You need to provide a valid image results grid wrapper reference."

    if !@pagination or @pagination.length is 0 then @pagination = false

    @page_size  ?= 12

    # Initialize search instance DOM
    @initDom()
    
    # Global vars
    @Query              = []    # Current query array
    @CarouselPhotoArray = []    # Carousel photo array

    @insertModuleDom(@initDom)

    # - - Event listeners - - #

    # If user presses enter
    @searchInput.on 'keydown', (e) =>
      return if not @valid_search_string()
      if e.keyCode is 13
        @newSearch @searchInput.val()

    # If user clicks search icon/button
    @searchBtn.on 'click', =>
      return if not @valid_search_string()
      @newSearch @searchInput.val()

    # If user clicks on grid results photos
    @list.on('click', @gridPhoto, @goToSlide)


  # - - Class Methods - - #

  insertModuleDom: (callback) =>

    # Search input dom
    searchDom = """
      <input type='text' placeholder='Search Flickr' tabindex='1' class='flickr-search-input' />
      <div class='status-wrap'>
        <i class='status spinner'></i>
        <i class='status magnifying-glass search-btn'></i>
      </div>
    """
    @search.html searchDom

    setTimeout( =>
      @searchInput.focus()
    , 10)

    # Carousel dom
    carouselDom = """
      <div class='arrows prev'></div>
      <div class='arrows next'></div>
      <div class='swiper-wrapper'></div>
      <div class='share-content'>
        <button class='share-btn'></button>
        <div class="share-buttons"></div>
      </div>
    """
    @carousel.html carouselDom

    callback()

  # When a user searches for a term(s)
  newSearch: (input) =>
    
    # Reset @page var to page 1
    @page = 1

    # Call Flickr API with search query and update results
    @flickrApi({ input, page_num: @page, page_size: @page_size, new_search: true }, @updateResults)


  # Makes query to Flickr endpoint with provided params
  # Also updates global reference vars
  # RETURNS:
  #   - success:  Arr [ {photo_object}, ... ]
  #   - fail:     false
  flickrApi: ({input, page_num, new_search}, callback) =>

    # Disable input during search request
    @toggleSearchStatus() # UI: now searching...

    # If searching new set of terms for first time,
    # trim unneccesary whitespace, and split input
    # into individual words array, to be passed into
    # query as tags
    if new_search
      @pagination.empty() if @pagination
      @Query = input.replace(/\s+/g,' ').trim().split(' ')

    # Dat ajax doh
    $.getJSON "https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&extras=owner_name&api_key=#{@api_key}&tags=#{@Query}&per_page=#{@page_size}&page=#{page_num}",

      (res) =>

        # Success
        if res.stat is 'ok'

          callback res.photos.photo.map (p) =>
            {
              lg: "https://farm#{p.farm}.staticflickr.com/#{p.server}/#{p.id}_#{p.secret}_b.jpg",
              sm: "https://farm#{p.farm}.staticflickr.com/#{p.server}/#{p.id}_#{p.secret}_m.jpg",
              ownername: p.ownername
            }

          @total_photos = res.photos.total

          if @total_photos > 1
            @arrows.addClass 'visible'
          else
            @arrows.removeClass 'visible'

          # If search returns no results
          if @total_photos < 1
            @share.removeClass 'visible'
            alert "How very niche! Unfortunately \"#{input.replace(/\s+/g,' ').trim().split(' ')}\" returned no results. Maybe try something a bit more... mainstream?"
            setTimeout( =>
              @searchInput.select()
            , 10)
          else 
            @share.addClass 'visible'

          # If there are enough results, 
          # AND this is a new search,
          # AND the pagination tag is defined
          # THEN init the pagination
          if res.photos.total > @page_size and new_search and @pagination isnt false then @doPagination parseInt(res.photos.pages)

          # Update UI: Done searching.
          return @toggleSearchStatus()

        # Fail
        else if res.stat is 'fail'
          alert "Oops! Something went wrong with Flickr! :/"
          return false


  # The @flickrApi() method callback
  updateResults: (results_array) =>

    # Populate and reinit carousel
    if @Carousel then @Carousel.destroy()

    @slideWrapper.empty()
    @list.empty()

    for photo, i in results_array

      @slideWrapper.append """
        <div class="swiper-slide">
          <div class="slide-image lazyload" data-bg="#{photo.lg}"></div>
          <div class="swiper-lazy-preloader"></div>
          <p class="credit">Photographer credit: #{photo.ownername}</p>
        </div>
      """

      @list.append "<li class='grid-photo lazyload' data-bg='#{photo.sm}' data-index='#{i}'></li>"


    autoplay_speed    = 5   # Speed in seconds to dwell on each image
    transition_speed  = 0.6 # What it says on tin. In seconds.

    # Init the carousel.
    # Note the different settings depending on whether on mobile or not
    @Carousel = new Swiper @carousel, {
      spaceBetween: 0,
      speed: transition_speed * 1000,
      loop: true,
      lazyLoading: true,
      autoplay: autoplay_speed * 1000,
      autoplayDisableOnInteraction: false,

      effect:     if @isMobile() then 'slide' else 'fade',
      nextButton: if @isMobile() then null else @nextBtn,
      prevButton: if @isMobile() then null else @prevBtn
    }


  doPagination: (total_pages) =>

    @new_pagination = document.createElement "ul"

    new_pag_id = "pag#{Date.now()}"

    @new_pagination.setAttribute('id', new_pag_id)
    @new_pagination.setAttribute('class', 'pagination-list')

    @pagination.html @new_pagination

    pag = @pagination.find("ul##{new_pag_id}")

    # Init pagination
    pag.twbsPagination {
      totalPages: total_pages
      visiblePages: 6
      onPageClick: @goToPage
      first: '&laquo;'
      prev: '&lt;'
      next: '&gt;'
      last: '&raquo;'
      startPage: 1
      initiateStartPageClick: false
      }


  goToPage: (e, page) =>
    @flickrApi({ page_num: page, new_search: false }, @updateResults)


  # User clicks on slide, display slide in carousel
  goToSlide: (e) =>
    @Carousel.slideTo( parseInt($(e.target).attr('data-index')) + 1, 0)


  # Toggles UI for search input
  toggleSearchStatus: ->
    @search.toggleClass 'searching'
    @searchInput.attr('disabled', @search.hasClass 'searching')

  isMobile: ->
    (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test( navigator.userAgent.toLowerCase() ))

  # Returns boolean status for search input validity
  valid_search_string: =>
    (@searchInput.val().length > 1)# or !@searchThrottle

  initDom: =>
    @arrows       = @carousel.find('.arrows')
    @nextBtn      = @carousel.find('.arrows.next')
    @prevBtn      = @carousel.find('.arrows.prev')
    @searchInput  = @search.find('input')
    @searchBtn    = @search.find('i.status.search-btn')
    @share        = @carousel.find('.share-content')

    @slideWrapper = @carousel.children('.swiper-wrapper')
    @gridPhoto    = @list.find('li.grid-photo')
