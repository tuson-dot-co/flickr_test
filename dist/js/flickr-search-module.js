(function() {
  var bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  this.FlickrSearch = (function() {
    function FlickrSearch(arg) {
      this.api_key = arg.api_key, this.search = arg.search, this.carousel = arg.carousel, this.list = arg.list, this.pagination = arg.pagination, this.page_size = arg.page_size;
      this.initDom = bind(this.initDom, this);
      this.valid_search_string = bind(this.valid_search_string, this);
      this.goToSlide = bind(this.goToSlide, this);
      this.goToPage = bind(this.goToPage, this);
      this.doPagination = bind(this.doPagination, this);
      this.updateResults = bind(this.updateResults, this);
      this.flickrApi = bind(this.flickrApi, this);
      this.newSearch = bind(this.newSearch, this);
      this.insertModuleDom = bind(this.insertModuleDom, this);
      if (this.api_key.length !== 32 || typeof this.api_key !== 'string') {
        throw "You need to provide a valid Flickr Api Key!";
      }
      if (!this.search || this.search.length === 0) {
        throw "You need to provide a valid search input wrapper reference.";
      }
      if (!this.carousel || this.carousel.length === 0) {
        throw "You need to provide a valid image carousel wrapper reference.";
      }
      if (!this.list || this.list.length === 0) {
        throw "You need to provide a valid image results grid wrapper reference.";
      }
      if (!this.pagination || this.pagination.length === 0) {
        this.pagination = false;
      }
      if (this.page_size == null) {
        this.page_size = 12;
      }
      this.initDom();
      this.Query = [];
      this.CarouselPhotoArray = [];
      this.insertModuleDom(this.initDom);
      this.searchInput.on('keydown', (function(_this) {
        return function(e) {
          if (!_this.valid_search_string()) {
            return;
          }
          if (e.keyCode === 13) {
            return _this.newSearch(_this.searchInput.val());
          }
        };
      })(this));
      this.searchBtn.on('click', (function(_this) {
        return function() {
          if (!_this.valid_search_string()) {
            return;
          }
          return _this.newSearch(_this.searchInput.val());
        };
      })(this));
      this.list.on('click', this.gridPhoto, this.goToSlide);
    }

    FlickrSearch.prototype.insertModuleDom = function(callback) {
      var carouselDom, searchDom;
      searchDom = "<input type='text' placeholder='Search Flickr' tabindex='1' class='flickr-search-input' />\n<div class='status-wrap'>\n  <i class='status spinner'></i>\n  <i class='status magnifying-glass search-btn'></i>\n</div>";
      this.search.html(searchDom);
      setTimeout((function(_this) {
        return function() {
          return _this.searchInput.focus();
        };
      })(this), 10);
      carouselDom = "<div class='arrows prev'></div>\n<div class='arrows next'></div>\n<div class='swiper-wrapper'></div>\n<div class='share-content'>\n  <button class='share-btn'></button>\n  <div class=\"share-buttons\"></div>\n</div>";
      this.carousel.html(carouselDom);
      return callback();
    };

    FlickrSearch.prototype.newSearch = function(input) {
      this.page = 1;
      return this.flickrApi({
        input: input,
        page_num: this.page,
        page_size: this.page_size,
        new_search: true
      }, this.updateResults);
    };

    FlickrSearch.prototype.flickrApi = function(arg, callback) {
      var input, new_search, page_num;
      input = arg.input, page_num = arg.page_num, new_search = arg.new_search;
      this.toggleSearchStatus();
      if (new_search) {
        if (this.pagination) {
          this.pagination.empty();
        }
        this.Query = input.replace(/\s+/g, ' ').trim().split(' ');
      }
      return $.getJSON("https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&extras=owner_name&api_key=" + this.api_key + "&tags=" + this.Query + "&per_page=" + this.page_size + "&page=" + page_num, (function(_this) {
        return function(res) {
          if (res.stat === 'ok') {
            callback(res.photos.photo.map(function(p) {
              return {
                lg: "https://farm" + p.farm + ".staticflickr.com/" + p.server + "/" + p.id + "_" + p.secret + "_b.jpg",
                sm: "https://farm" + p.farm + ".staticflickr.com/" + p.server + "/" + p.id + "_" + p.secret + "_m.jpg",
                ownername: p.ownername
              };
            }));
            _this.total_photos = res.photos.total;
            if (_this.total_photos > 1) {
              _this.arrows.addClass('visible');
            } else {
              _this.arrows.removeClass('visible');
            }
            if (_this.total_photos < 1) {
              _this.share.removeClass('visible');
              alert("How very niche! Unfortunately \"" + (input.replace(/\s+/g, ' ').trim().split(' ')) + "\" returned no results. Maybe try something a bit more... mainstream?");
              setTimeout(function() {
                return _this.searchInput.select();
              }, 10);
            } else {
              _this.share.addClass('visible');
            }
            if (res.photos.total > _this.page_size && new_search && _this.pagination !== false) {
              _this.doPagination(parseInt(res.photos.pages));
            }
            return _this.toggleSearchStatus();
          } else if (res.stat === 'fail') {
            alert("Oops! Something went wrong with Flickr! :/");
            return false;
          }
        };
      })(this));
    };

    FlickrSearch.prototype.updateResults = function(results_array) {
      var autoplay_speed, i, j, len, photo, transition_speed;
      if (this.Carousel) {
        this.Carousel.destroy();
      }
      this.slideWrapper.empty();
      this.list.empty();
      for (i = j = 0, len = results_array.length; j < len; i = ++j) {
        photo = results_array[i];
        this.slideWrapper.append("<div class=\"swiper-slide\">\n  <div class=\"slide-image lazyload\" data-bg=\"" + photo.lg + "\"></div>\n  <div class=\"swiper-lazy-preloader\"></div>\n  <p class=\"credit\">Photographer credit: " + photo.ownername + "</p>\n</div>");
        this.list.append("<li class='grid-photo lazyload' data-bg='" + photo.sm + "' data-index='" + i + "'></li>");
      }
      autoplay_speed = 5;
      transition_speed = 0.6;
      return this.Carousel = new Swiper(this.carousel, {
        spaceBetween: 0,
        speed: transition_speed * 1000,
        loop: true,
        lazyLoading: true,
        autoplay: autoplay_speed * 1000,
        autoplayDisableOnInteraction: false,
        effect: this.isMobile() ? 'slide' : 'fade',
        nextButton: this.isMobile() ? null : this.nextBtn,
        prevButton: this.isMobile() ? null : this.prevBtn
      });
    };

    FlickrSearch.prototype.doPagination = function(total_pages) {
      var new_pag_id, pag;
      this.new_pagination = document.createElement("ul");
      new_pag_id = "pag" + (Date.now());
      this.new_pagination.setAttribute('id', new_pag_id);
      this.new_pagination.setAttribute('class', 'pagination-list');
      this.pagination.html(this.new_pagination);
      pag = this.pagination.find("ul#" + new_pag_id);
      return pag.twbsPagination({
        totalPages: total_pages,
        visiblePages: 6,
        onPageClick: this.goToPage,
        first: '&laquo;',
        prev: '&lt;',
        next: '&gt;',
        last: '&raquo;',
        startPage: 1,
        initiateStartPageClick: false
      });
    };

    FlickrSearch.prototype.goToPage = function(e, page) {
      return this.flickrApi({
        page_num: page,
        new_search: false
      }, this.updateResults);
    };

    FlickrSearch.prototype.goToSlide = function(e) {
      return this.Carousel.slideTo(parseInt($(e.target).attr('data-index')) + 1, 0);
    };

    FlickrSearch.prototype.toggleSearchStatus = function() {
      this.search.toggleClass('searching');
      return this.searchInput.attr('disabled', this.search.hasClass('searching'));
    };

    FlickrSearch.prototype.isMobile = function() {
      return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase());
    };

    FlickrSearch.prototype.valid_search_string = function() {
      return this.searchInput.val().length > 1;
    };

    FlickrSearch.prototype.initDom = function() {
      this.arrows = this.carousel.find('.arrows');
      this.nextBtn = this.carousel.find('.arrows.next');
      this.prevBtn = this.carousel.find('.arrows.prev');
      this.searchInput = this.search.find('input');
      this.searchBtn = this.search.find('i.status.search-btn');
      this.share = this.carousel.find('.share-content');
      this.slideWrapper = this.carousel.children('.swiper-wrapper');
      return this.gridPhoto = this.list.find('li.grid-photo');
    };

    return FlickrSearch;

  })();

}).call(this);
