(function() {
  var SearchPage,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  SearchPage = (function() {
    function SearchPage() {
      this.initDom = bind(this.initDom, this);
      this.initDom();
      $((function(_this) {
        return function() {
          return _this.FlickrSearch = new FlickrSearch({
            api_key: 'd2011d602fa5b6d6f61e09a72457fc12',
            search: _this.search1,
            carousel: _this.carousel1,
            list: _this.list1,
            pagination: _this.pagination1
          });
        };
      })(this));
    }

    SearchPage.prototype.initDom = function() {
      this.search1 = $('#search-1');
      this.carousel1 = $('#carousel-1');
      this.list1 = $('#results-grid-1');
      return this.pagination1 = $('#pagination-1');
    };

    return SearchPage;

  })();

  this.SearchPage = new SearchPage;

}).call(this);
