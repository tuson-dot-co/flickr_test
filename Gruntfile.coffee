module.exports = (grunt) ->

  grunt.initConfig
    sass:
      dist:
        options:
          style: 'expanded'
        files:
          'dist/css/styles.css': 'src/css/styles.sass',
          'dist/css/flickr-search-module.css': 'src/css/flickr-search-module.sass'

    coffee:
      compile:
        files:
          'dist/js/script.js': 'src/js/script.coffee',
          'dist/js/flickr-search-module.js': 'src/js/flickr-search-module.coffee'

    haml:
      dist:
        files:
          'dist/index.html': 'src/index.haml'

    imagemin:
      files:
        expand: true,
        cwd: 'src/img',
        src: ['**/*.{png,jpg,gif,svg}'],
        dest: 'dist/img'

    watch:
      scripts:
        files: 'src/**/*.coffee',
        tasks: ['coffee']
      styles:
        files: 'src/**/*.sass',
        tasks: ['sass']
      html:
        files: 'src/**/*.haml',
        tasks: ['haml']
      img:
        files: 'src/**/*.img',
        tasks: ['imagemin']

    uglify:
      module:
        files:
          'dist/js/flickr-search-module.min.js': ['src/js/vendor/jquery-2.2.3.min.js', 'src/js/vendor/swiper.min.js', 'src/js/vendor/lazysizes.min.js', 'src/js/vendor/ls.unveilhooks.min.js', 'src/js/vendor/jquery.twbsPagination.min.js', 'dist/js/flickr-search-module.js']

    cssmin:
      options:
        shorthandCompacting: false,
        roundingPrecision: -1
      target:
        files:
          'dist/css/flickr-search-module.min.css': ['src/css/vendor/swiper.min.css', 'dist/css/flickr-search-module.css']

  grunt.loadNpmTasks 'grunt-contrib-sass'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-haml'
  grunt.loadNpmTasks 'grunt-contrib-imagemin'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  # Default task(s).
  grunt.registerTask 'default', ['sass', 'coffee', 'haml', 'imagemin', 'uglify', 'cssmin', 'watch']