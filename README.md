# Joe Tuson | Ticketmaster Test

This Flickr Search prototype can be demoed via the ***dist/index.html file***:
```sh
> flickr_test/dist/index.html
```

I have included the whole project, including ***src*** folder and ***Gruntfile*** so you can see my actual code and how I use preprocessors.

### Built with:
- [HAML] - for markup 
- [SASS] - for styling
- [Coffeescript] - for scripting
- [Grunt] - for task automation/compilation
- [jQuery] - for DOM manipulation*
- [Swiper.js] - for the image carousel*
- [lazysizes] - for lazyloading*
- [twbs-pagination] - for the pagination*

### *Why those plugins/libraries?
Wherever possible, rather than reinventing the wheel, I will use plugins that I know I can rely on to do the heavy-lifting on functional components like image carousels, pagination, lazyloading, etc. This of course makes the development process more efficient, allowing me to spend more time on the app itself.

### My thinking behind my solution
Having studied the design, I identified certain necessary elements that form the make up of the prototype, these being:
- A search input
- An image carousel with next/prev buttons
- A results grid displaying thumbnails
- A pagination widget

As I saw it, the module would want to provide those functionalities with as little up-front effort as possible, but still be flexible from a styling point of view. So I designed the module to accept DOM wrappers on initialisation into which it would dynamically inject everything else via javascript. The module requires it's own stylesheet for some basic foundational styles, but leaves the vast majority of the look and feel to the implementor.

### How to use the module:
Include the module stylesheet at the head of the document:
```sh
<link href='./css/flickr-search-module.min.css' rel='stylesheet' type='text/css'>
```
...and the module script at the bottom of the document, before your other scripts:
```sh
<script src='./js/flickr-search-module.min.js'></script>
```
In your HTML, define four wrapper divs for the aforementioned functionalities, then, in your JS create a new instance of the ***FlickrSearch*** module, providing the DOM references to those elements:
```sh
var myFlickrSearch = new FlickrSearch({
    api_key:    '<your_flickr_rest_api_key>',
    search:     $("#my-search-box"),
    carousel:   $("#my-carousel"),
    list:       $("#my-results-grid"),
    pagination: $("#the-pagination")
});
```

And you're done!

___

### Other things to note:
- I spent a small amount of time making the prototype slightly responsive. The carousel will, on mobile devices, swipe rather than fade.
- Something I didn't quite deal with was the referencing of the various functional image assets (loading gif, share button, next/prev arrows). These in practice would need a solution like being hosted remotely on a CDN with the ability to offer an override.

[//]: # (Reference links:)
   [HAML]: <http://haml.info/>
   [SASS]: <http://sass-lang.com/>
   [Coffeescript]: <http://coffeescript.org/>
   [Grunt]: <http://gruntjs.com/>
   [jQuery]: <https://jquery.com/>
   [Swiper.js]: <http://idangero.us/swiper/>
   [lazysizes]: <https://github.com/aFarkas/lazysizes>
   [twbs-pagination]: <https://github.com/esimakin/twbs-pagination>
